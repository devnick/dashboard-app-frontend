'use strict';

/**
 * @ngdoc overview
 * @name dashboardApp
 * @description
 * # dashboardApp
 *
 * Main module of the application.
 */
angular
    .module('dashboardApp', [
        'ngAnimate',
        'ngCookies',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'chart.js',
        'ngDialog'
    ])
    .config(function($urlRouterProvider) {

        // Fallback route
        $urlRouterProvider.otherwise('/');

    });