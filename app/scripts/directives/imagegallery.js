'use strict';

/**
 * @ngdoc directive
 * @name dashboardApp.directive:imageGallery
 * @description
 * # imageGallery
 */
 angular.module('dashboardApp')
    .directive('imageGallery', function () {
        return {
            templateUrl: 'views/common/image-gallery.html',
            replace: true,
            restrict: 'E',
            scope: {
                data: '='
            },
            controller: ['$scope', 'ngDialog', function($scope, ngDialog) {

                // Private Methods
                function openGalleryModal(img) {
                    ngDialog.open({
                        data: {
                            active: img,
                            all: $scope.data
                        },
                        template: 'views/common/gallery-modal.html',
                        controller: ['$scope', function($scope) {
                            $scope.changeActiveImage = function(img) {
                                $scope.ngDialogData.active = img;
                            };
                        }]
                    });
                }

                // Public Methods
                $scope.openGalleryModal = openGalleryModal;
            }]
        };
    });
