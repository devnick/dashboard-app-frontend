'use strict';

/**
 * @ngdoc directive
 * @name dashboardApp.directive:userTable
 * @description
 * # userTable
 */
angular.module('dashboardApp')
    .directive('userTable', function () {
        return {
            templateUrl: 'views/common/user-table.html',
            replace: true,
            scope: {
                data: '='
            },
            restrict: 'E',
            link: function postLink(scope) {
                console.log(scope);
            }
        };
    });
