'use strict';

/**
 * @ngdoc directive
 * @name dashboardApp.directive:progressBars
 * @description
 * # progressBars
 */
 angular.module('dashboardApp')
    .directive('progressBars', function () {
        return {
            templateUrl: 'views/common/progress-bars.html',
            restrict: 'E',
            replace: true,
            scope: {
                data: '='
            }
        };
    });
