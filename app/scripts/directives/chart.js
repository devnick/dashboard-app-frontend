'use strict';

/**
 * @ngdoc directive
 * @name dashboardApp.directive:chart
 * @description
 * # chart
 */
angular.module('dashboardApp')
  .directive('chart', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the chart directive');
      }
    };
  });
