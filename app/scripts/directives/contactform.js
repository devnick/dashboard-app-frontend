'use strict';

/**
 * @ngdoc directive
 * @name dashboardApp.directive:contactForm
 * @description
 * # contactForm
 */
angular.module('dashboardApp')

    .directive('contactForm', function () {
        return {
            templateUrl: 'views/common/contact-form.html',
            scope: {
                data: '='
            },
            replace: true,
            restrict: 'E',
            link: function postLink(scope, element, attrs) {

            }
        };
    });
