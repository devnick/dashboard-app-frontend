'use strict';

/**
 * @ngdoc filter
 * @name dashboardApp.filter:progressCheck
 * @function
 * @description
 * # progressCheck
 * Filter in the dashboardApp.
 */
angular.module('dashboardApp')

    .filter('progressCheck', function () {
        return function (input) {
            if (input < 40) {
                return 'danger';
            } else if(input > 40 && input <70) {
                return 'warning';
            } else if(input > 70) {
                return 'success';
            } else {
                return '';
            }
        };
    });
