'use strict';

/**
 * @ngdoc filter
 * @name dashboardApp.filter:translateText
 * @function
 * @description
 * # translateText
 * Filter in the dashboardApp.
 */
angular.module('dashboardApp')

    .filter('translateText', function () {
        return function (input) {
            return input + ': the filter is working!';
        };
    });
