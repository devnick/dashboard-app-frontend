'use strict';

/**
 * Session States
 */
angular.module('dashboardApp')

    .config(function ($stateProvider) {

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'views/session/login.html',
                controller: 'SessionCtrl as session'
            });
    });