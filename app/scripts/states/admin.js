'use strict';

/**
 * Admin States
 */
angular.module('dashboardApp')

    .config(function ($stateProvider) {

        $stateProvider

            // Base state
            .state('admin', {
                abstract: true,
                url: '/',
                templateUrl: 'views/admin/base.html',
                controller: 'DashboardCtrl as dashboard'
            })

            // Dashboard
            .state('admin.dashboard', {
                url: '',
                templateUrl: 'views/admin/dashboard.html'
            })
            .state('admin.charts', {
                url: 'charts',
                templateUrl: 'views/admin/charts.html'
            })
            .state('admin.galleries', {
                url: 'galleries',
                templateUrl: 'views/admin/galleries.html'
            })
            .state('admin.uploads', {
                url: 'uploads',
                templateUrl: 'views/admin/uploads.html'
            })
            .state('admin.stats', {
                url: 'stats',
                templateUrl: 'views/admin/stats.html'
            })
            .state('admin.profile', {
                url: 'profile',
                templateUrl: 'views/admin/profile.html'
            })
            .state('admin.lounge', {
                url: 'lounge',
                templateUrl: 'views/admin/lounge.html'
            });
    });