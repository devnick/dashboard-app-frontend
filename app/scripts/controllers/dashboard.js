'use strict';

/**
 * @ngdoc function
 * @name dashboardAppApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the dashboardAppApp
 */
 angular.module('dashboardApp')

 .controller('DashboardCtrl', function ($scope) {

    // Instantiate controller POJO
    var dashboard = this;

    dashboard.chartData = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
    ];

    // Private Methods
    function getImages() {
        var arr = [];
        for (var i = 16; i >= 1; i--) {
            var base = Math.floor(Math.random() * 1000) + 400;
            arr.push('http://fillmurray.com/' + base + '/' + base);
        }
        return arr;
    }

    function generateChartData() {
        var arr = [];
        for (var i = 2; i >= 1; i--) {
            var data = [];
            for (var r = 7; r >= 1; r--) {
                data.push(Math.floor(Math.random() * 100) + 1);
            }
            arr.push(data);
        }
        return arr;
    }

    dashboard.chartData = generateChartData();

    // Line Chart
    dashboard.chart = {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        options: {
            scaleShowGridLines : false,
            scaleShowLabels : false,
            bezierCurve : true,
            pointDot : true,
            pointDotRadius : 3,
            pointDotStrokeWidth : 3,
            scaleOverride : true,
            scaleSteps : 5,
            scaleStepWidth : 20,
            responsive : true,
            showTooltips: true,
            tooltipFontSize: 16,
            tooltipYPadding: 12,
            tooltipXPadding: 12,
            tooltipCornerRadius: 3,
            tooltipFillColor: '#111f31',
            scaleGridLineColor : 'rgba(0, 0, 0, 0)',
            scaleFontColor: '#526c8d'
        },
        colors: [
            {
                fillColor: 'rgba(155, 89, 182, 0.08)',
                strokeColor: 'rgba(155, 89, 182, 1)',
                pointColor: 'rgba(155, 89, 182, 0.4)',
                pointStrokeColor: 'rgba(155, 89, 182, 1)',
                pointHighlightFill: 'rgba(155, 89, 182, 1)',
                pointHighlightStroke: 'rgba(155, 89, 182, 1)'
            },
            {
                fillColor: 'rgba(96, 211, 229, 0.08)',
                strokeColor: 'rgba(96, 211, 229, 1)',
                pointColor: 'rgba(96, 211, 229, 0.4)',
                pointStrokeColor: 'rgba(96, 211, 229, 1)',
                pointHighlightFill: 'rgba(96, 211, 229, 1)',
                pointHighlightStroke: 'rgba(96, 211, 229, 1)'
            }
        ],
        barColors: [
            {
                fillColor: 'rgba(155, 89, 182, 0.8)',
                strokeColor: 'rgba(155, 89, 182, 1)',
                pointColor: 'rgba(155, 89, 182, 0.4)',
                pointStrokeColor: 'rgba(155, 89, 182, 1)',
                pointHighlightFill: 'rgba(155, 89, 182, 1)',
                pointHighlightStroke: 'rgba(155, 89, 182, 1)'
            },
            {
                fillColor: 'rgba(96, 211, 229, 0.8)',
                strokeColor: 'rgba(96, 211, 229, 1)',
                pointColor: 'rgba(96, 211, 229, 0.4)',
                pointStrokeColor: 'rgba(96, 211, 229, 1)',
                pointHighlightFill: 'rgba(96, 211, 229, 1)',
                pointHighlightStroke: 'rgba(96, 211, 229, 1)'
            }
        ],
        onClick: function (points, evt) {
            console.log(points, evt);
        }
    };

    // Doughnut Chart
    dashboard.circle = {
        data: [100, 40],
        labels: ['Total', 'Average'],
        options: {
            animationEasing: 'easeOut',
            animationSteps: 40
        }
    };

    // Faked progress values
    dashboard.progressItems = [
        { title: 'Value 1', value: 34 },
        { title: 'Value 2', value: 60 },
        { title: 'Value 3', value: 95 },
        { title: 'Value 4', value: 72 },
        { title: 'Value 5', value: 68 },
        { title: 'Value 6', value: 44 },
        { title: 'Value 7', value: 80 },
        { title: 'Value 8', value: 41 }
    ];

    // Faked users array
    dashboard.users = [
        { name: 'Nicholas Hebert', email: 'nick@devnick.com', role: 'Administrator' },
        { name: 'Test User 1', email: 'nick@devnick.com', role: 'Administrator' },
        { name: 'Test User 2', email: 'testuser1@testdomain.com', role: 'Administrator' },
        { name: 'Test User 3', email: 'testuser1@testdomain.com', role: 'Creator' },
        { name: 'Test User 4', email: 'testuser1@testdomain.com', role: 'User' },
        { name: 'Test User 5', email: 'testuser1@testdomain.com', role: 'User' },
        { name: 'Test User 6', email: 'testuser1@testdomain.com', role: 'Administrator' }
    ];

    // Public Assignments
    dashboard.images = getImages();

    dashboard.randomize = function () {
        dashboard.chartData = dashboard.chartData.map(function (data) {
            return data.map(function (y) {
                return Math.floor(Math.random() * 100) + 1;
            });
        });
    };
});