'use strict';

describe('Filter: progressCheck', function () {

  // load the filter's module
  beforeEach(module('dashboardApp'));

  // initialize a new instance of the filter before each test
  var progressCheck;
  beforeEach(inject(function ($filter) {
    progressCheck = $filter('progressCheck');
  }));

  it('should return the input prefixed with "progressCheck filter:"', function () {
    var text = 'angularjs';
    expect(progressCheck(text)).toBe('progressCheck filter: ' + text);
  });

});
