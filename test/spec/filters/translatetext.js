'use strict';

describe('Filter: translateText', function () {

  // load the filter's module
  beforeEach(module('dashboardApp'));

  // initialize a new instance of the filter before each test
  var translateText;
  beforeEach(inject(function ($filter) {
    translateText = $filter('translateText');
  }));

  it('should return the input prefixed with "translateText filter:"', function () {
    var text = 'angularjs';
    expect(translateText(text)).toBe('translateText filter: ' + text);
  });

});
